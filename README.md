## ssh helper [![pipeline status](https://gitlab.com/gitlab-cd/ssh-template/badges/master/pipeline.svg)](https://gitlab.com/gitlab-cd/ssh-template/pipelines/)
ssh helper ease remote connection from gilab-ci to your ssh host.  
This library provides two commands, **ssh_init** (optional) and **ssh_run**.  


### setup 
- First, Add the private key (i.e: SSH_PRIVATE_KEY_FILE or SSH_TOKEN) as a variable to your gitlab project or group.  
![](https://docs.gitlab.com/ee/ci/variables/img/variables.png)
Note : you should not flag this variable as Protected if you want to use this variable with non-protected branch, otherwise ssh will fail.

- Then include the ssh.yml script from your .gitlab-ci.yml
```yaml
include: 'https://gitlab.com/pskpatil/gilab-cicd-ssh-template/raw/master/ssh.yml'
````

### ssh_init & ssh_run
Simply authenticate and execute remote command on a single line.
> **ssh_init** $SSH_PRIVATE_KEY_FILE HOSTNAME<br/>
> **ssh_run** USER@HOSTNAME COMMAND  

```yaml
include: 'https://gitlab.com/pskpatil/gilab-cicd-ssh-template/raw/master/ssh.yml'

clean:
stage: build
script:
- ssh_init $SSH_PRIVATE_KEY_FILE myownserver.com
- ssh_run root@myownserver.com" "rm -rf .cache"
```


###  Integration note
By design in ssh.yml, the ssh helper is initialized during the `before_script` gitlab step.  
Note it won't work anymore if you define your own `before_script` section in  `.gitlab-ci.yml`.  
If you want to define your own `before_script`, be sure you add a call to `ssh_helper` in that section :  
```yaml
before_script:
- *ssh_helper
- echo "then, your own instructions..."
```


###  Additional note
By configuration the gitlab-runner may not persist the OS per stage, in other words every stage might  see new OS being inititated. (Just as in my case).

So, you may need a git_init for every stage.