echo ..:: ssh-init.sh ::..
test -n "$SSH_PRIVATE_KEY_FILE" || ( echo Fatal : missing variable SSH_PRIVATE_KEY_FILE && exit 1 )
test -n "$SSH_KNOWN_HOSTS" || echo Warn : missing variable SSH_KNOWN_HOSTS

mkdir -p ~/.ssh
chmod 700 ~/.ssh

which ssh-agent ||  ( apk --update add openssh-client ) # its alpine
eval $(ssh-agent -s)
cat "$SSH_PRIVATE_KEY_FILE" | tr -d '\r' | ssh-add - > /dev/null

if [ -n $SSH_KNOWN_HOSTS ]; then
    ssh-keygen -R "$SSH_KNOWN_HOSTS" || echo
    ssh-keyscan -H "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
    chmod 644 ~/.ssh/known_hosts
fi

